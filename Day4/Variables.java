 
package Day4;

/**
 *
 * @author SURAJ BANIYA
 */
public class Variables {
    int num; //global variable or instance variable or class variable
    static int num2;//static variable
    enum DAYS{SUN,MON,TUE}
    public static void main(String[] args) {
       /* int num3;//local variable;
         byte b=10;
         b=20;
         
         char c='S';
         c='T';
         boolean res=true;
         res=false;
         System.out.println(b);
          num3=b;
          System.out.println(num3);
         System.out.println(c);
         System.out.println(res);
               */
        DAYS d1;
        d1=DAYS.MON;
        System.out.println(d1);
        //type casting implict
        byte bn=10;
        int in=bn;
        double dn=in;
        System.out.println(dn);
        //type cating explict
        dn=789455;
        in=(int)dn;
        bn=(byte)in;
        System.out.println(bn);
        
        
    }
}
