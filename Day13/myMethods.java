/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Day13;

import java.util.Scanner;

/**
 *
 * @author SURAJ BANIYA
 */
public class myMethods {
    public void f1(){
        System.out.println("hello");
        
    }
     //acessess specifier-public,no return type,name of method paramertised
    public  void f2(String message){
        System.out.println("message!!! ::"+message);
    }
     //acessess specifier-public, return type,name of method no- paramertised
    public String readString(){
        String str2;
        str2= new Scanner(System.in).nextLine();
        return str2;
    }
    //acessess specifier-public, return type,name of method - paramertised
    public double doAdd(double n1,double n2){
        double sum;
        sum=n1+n2;
        return (sum);
        
    }
    
}
