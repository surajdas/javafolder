/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Day13;

import java.util.Scanner;

/**
 *
 * @author SURAJ BANIYA
 */
public class Methods {
    //acessess specifier-public, no return type,name of method - paramertised
    public void printMessage(String msg){
        System.out.print(msg);
        
    }
    //acessess specifier-public, return type,name of method - paramertised
    public void printMessage(String label1,String value){
        System.out.print(label1+","+value);
    }
    public  void newLine(){
        System.out.println("     ");
    }
    //acessess specifier-public, return type,name of method no - paramertised
    public int readInteger(){
        int temp = new Scanner(System.in).nextInt();
        return (temp);
       
    }
   
    public  int doAdd(int n1,int n2){
         return (n1+n2);
    }
}
