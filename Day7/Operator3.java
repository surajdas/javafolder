/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Day7;

/**
 *
 * @author SURAJ BANIYA
 */
public class Operator3 {
    //Relational operator
    //numeric type
     public static void main(String[] args) {
        int num1,num2;
        num1=65;
        num2=65;
        boolean res;
        res=(num1==num2);
         System.out.println(res);
         res=(num1!=num2);
         System.out.println(res);
         res=(num1>num2);
         System.out.println(res);
         res=(num1<num2);
         System.out.println(res);
         res=(num1>=num2);
         System.out.println(res);
         res=(num1<=num2);
         System.out.println(res);
         
         //boolean type
         boolean b1,b2,res2;
         b1=true;
         b2=false;
         res2=(b1==b2);
         System.out.println(  );
         System.out.println(res2);
         res2=(b1!=b2);
         System.out.println(  );
         System.out.println(res2);
         
         
         //character type
         char ch1,ch2;
         boolean res3;
         ch1='S';
         ch2='D';
         
         res3=(ch1==ch2);
         System.out.println(res3);
         
          res3=(ch1!=ch2);
         System.out.println(res3);
         
          res3=(ch1>ch2);
         System.out.println(res3);
         
          res3=(ch1<ch2);
         System.out.println(res3);
         
          res3=(ch1>=ch2);
         System.out.println(res3);
         
          res3=(ch1<=ch2);
         System.out.println(res3);
         
         //string 
         String str1,str2;
         boolean res4;
         str1="nepal";
         str2="Nepal";
         
         res4=(str1==str2);
         System.out.println("\t"+res4);
         
          res4=(str1!=str2);
          
         System.out.println(res4);
         
         res4=str1.equals(str2);
         System.out.println(res4);
         
         //object
         Object ob1,ob2;
         boolean res5;
         ob1=34;
         ob2="kathmandu";
         res5=ob1.equals(ob2);
         System.out.println("\n"+res5);
         
           res5=(ob1!=ob2);
         System.out.println("\n"+res5);
         
         
         //NOT operator
         boolean res6,res7;
         
         res6=true;
         res7=res6;
         System.out.println(res);
         
          res7=!(res6);
         System.out.println(res);
          
         
         
        
         
         
    }
     
}
