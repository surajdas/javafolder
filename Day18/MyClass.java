/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Day18;

/**
 *
 * @author SURAJ BANIYA
 */
import javax.swing.JFrame;//liubrary class
public class MyClass {
    JFrame frame;

    //creating default constructor
    public MyClass() {
        frame=new JFrame();//initialise
        frame.setTitle("First window");
        frame.setSize(350,200);//setSize 
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
         frame.setVisible(true);
    }
    //main method for testing
    public static void main(String[] args){
        new MyClass();
    }
    //declare
    /*
    constructors
    setters
    getters
    process methods
    toString
    */
    
    
}
