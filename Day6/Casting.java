/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Day6;

/**
 *
 * @author SURAJ BANIYA
 */
public class Casting {
    public static void main(String[] args) {
        
        /**
         * basic types to string types
         * And string types to basic types
         * wrapper class is used for the conversion from string to basic types 
         * or basic types to string types
         * numeric to string conversion
         * String_variable=wrapper class.toString(Numeric_variable)
         * String to numeric_variable
         * numeric_variable=wrapper class.parseType(string_value)
         * it is done for only numeric basic types like byte short, integer ,long,float ,double
         */
        //byte to string conversion
        byte bn=45;
        String str;
        str=Byte.toString(bn);
        System.out.println(bn);
        
        //string to byte conversion
        String st="54";
        byte byt;
        byt=Byte.parseByte(st);
        System.out.println(byt);
        //int to string
        int x=456;
         String str3;
         str3=Integer.toString(x);
         System.out.println(x);
         
         /* string to integer*/
         String p="45612";
         int Y;
         Y=Integer.parseInt(p);
         System.out.println(Y);
         
         //short to string
         short sh=635;
         String stri;
         stri=Short.toString(sh);
         System.out.println(sh);
         //string to short
         String strin="4567";
         short shk;
         shk=Short.parseShort(strin);
         System.out.println("strin");
         
         //float to string
         float fl=1002.456f;
         String tring;
         tring=Float.toString(fl);
         System.out.println(tring);
          
         //string to float
        String ring ="45678.000045";
        float flt;
        flt=Float.parseFloat(ring);
        System.out.println(ring);
        
        //double to string
        double db=456789.0004656;
        String ng;
        ng=Double.toString(db);
        System.out.println(ng);
        
        //string to double 
        String strg="456789.00004654";
        double db2;
        db2=Double.parseDouble(strg);
        System.out.println(strg);
         
         
         
        
        
    }
}
