/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Day6;

/**
 *
 * @author SURAJ BANIYA
 */
public class IncrementOper {
    public static void main(String[] args) {
        //increment
        int num1;
        num1=10;
        System.out.println(num1);
        
        num1++;//post
        System.out.println(num1);
        num1+=63;//short hand assignment operator
        System.out.println(num1);
        
        ++num1;//pre increment operator
        System.out.println(++num1);
        System.out.println(  );
        System.out.println(num1++);
        
        //conditional operator 
        int num3,num4,num5;
        boolean res1;
        num3=60;
        num4=45;
        
        res1=(num3>=num4);
        System.out.println(res1);
        num5=(num3>num4)?num3:num4;
        System.out.println(num5);
    }
}
