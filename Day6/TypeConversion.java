/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Day6;

/**
 *
 * @author SURAJ BANIYA
 */
public class TypeConversion {
    public static void main(String[] args) {
        
        //type conversion
         
        byte bn;
        bn=23;
        System.out.println(bn);
        String str;
        bn=123;
        
        //byte -only store (Basic Type )//Assign,update, access
        //Byte-Store string to Byte,byte to string Assign,update,acess
        Byte bn1=123;
        bn1=12;//update
        System.out.println(bn1);//Assign
        String str2=Byte.toString(bn1);
        System.out.println(str2);
        int in=505;
        System.out.println(in);
        String str4;
        str4=Integer.toString(in);
        System.out.println(str4);
        str4="45678";
         in=Integer.parseInt(str4);
         System.out.println(in);
         //double conversion
         //string to double
         String strn="4561";
         double db;
          db=Double.parseDouble(strn);
         System.out.println(db);
         //double to string
         double db2=456.1234;
         String str6=Double.toString(db2);//
         
         
    }
}
