/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Day111;

/**
 *
 * @author SURAJ BANIYA
 */
public class StudyOfMath {
      double num1;
    double num2;
    double num3;
   int x;
   public StudyOfMath(){
       
   }
   public void setX()
   {
       this.x=x;
   }
   public int getX()
   {
       return this.x;
   }
   public void setNum1(double n1)
   {
       this.num1=n1;
   }
   public double getNum1()
   {
       return this.num1;
   }
   public void setNum2(double n2)
   {
       this.num2=n2;
       
   }
   public double getNum2()
   {
       return this.num2;
   }
   public void setNum3(double n3)
   {
     this.num3=n3;
   }
   public double getNum3()
   {
       return this.num3;
   }
   
   public void doAdd()
   {
      double temp=this.num1+this.num2;
      this.setNum3(temp);
   }
    
   public String toString(){
       return (this.num1+","+this.num2+","+this.num3);
   }
   public double doDiff(){
       double temp=Math.abs(this.num1-this.num2);
       this.setNum3(temp);
       return this.num3;
      
   }
   public double doMultp(){
       double temp=this.num1*this.num2;
       this.setNum3(temp);
       return this.num3;
   }
   public double doPow()
   {
       double temp=Math.pow(this.num1,this.num2);
       this.setNum3(temp);
       return this.num3;
   }
   public double doTrig()
   {
       double temp=Math.cos(this.num1);
       double temp1=Math.acos(this.num2);
       this.setNum3(temp);
       return (this.num3);
       
   }
  public double doTan()
  {
      double temp=Math.tan(this.num2);
      this.setNum3(temp);
      return this.num3;
  }
  public double doSin(){
      double temp=Math.sin(this.num2);
      this.setNum3(temp);
      return this.num3;
  }
  public double doCubeRoot()
  {
     double temp=Math.cbrt(this.num2);
     this.setNum3(temp);
     return this.num3;
  }
  public double doSqrt()
  {
      double temp=Math.sqrt(this.num1);
      this.setNum3(temp);
      return this.num3;
  }
  public double doHyps(){
      double temp=Math.hypot(this.num1, this.num2);
      this.setNum3(temp);
      return this.num3;
  }
  public double doScalb()
  {
      double temp=Math.scalb(this.num1, this.x);
      this.setNum3(temp);
      return this.num3;
              
  }
    
}