/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Day17;

/**
 *
 * @author SURAJ BANIYA
 */
public class Break {
    public static void main(String[] args) {
       /* for(int i=1;i<5;i++)
        {
            if(i==3)
                break;
                System.out.println(i);
                
            
        }*/
      /*  for(int i=1;i<10;i++)
        {
            if(i==4)
                continue;
            System.out.println(i);
        }*/
      /**  int[] b={10,20,30,40,50};
        for(int i=0;i<b.length;i++) 
            /**
             * equivalent of the array type for loop::
             * for(int i:b)
             * 
             
            System.out.println(b[i]);
        int[]a={12,2,3,4};
        for(int i:a) 
            System.out.println(i);
        */
        
        int[] array=new int[10];
        array[0]=1;
        array[1]=10;
        array[2]=20;
        array[3]=30;
        array[4]=40;
        array[5]=50;
        array[6]=45;
        array[7]=49;
        array[8]=65;
        array[9]=423;
         for(int i:array)
             System.out.println(i);
    }
    
}
