/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Day11;

import Day10.*;

/**
 *
 * @author SURAJ BANIYA
 */
public class Calculation {
   double num1,num2,num3;
   
   public Calculation()
   {
       this.num1=0;
       this.num2=4;
       this.num3=5;
   }
   public Calculation(double num1,double num2)
   {
       this.num1=num1;
       this.num2=num2;
        this.num3=0;
   }
   public void SetNum1(double num1)
   {
       this.num1=num1;
   }
   public double getNum1()
   {
       return this.num1;
   }
    public void SetNum2(double num2)
   {
       this.num2=num2;
   }
   public double getNum2()
   {
       return this.num2;
   }
    public void SetNum3(double num3)
   {
       this.num3=num3;
   }
   public double getNum3()
   {
       return this.num3;
   }
   public void doAdd()
   {
       double temp=this.num1+this.num2;
       this.SetNum3(temp);
   }
   //Subtraction
   public void doDiiff()
   {
       double temp1=this.getNum1()-this.getNum2();
       this.SetNum3(temp1);
       temp1=Math.abs(temp1);//-ve to +ve
   }
   //multiplication
   public void doMultp()
   {
       double temp2=this.getNum1()*this.getNum2();
       this.SetNum3(temp2);
   }
    public void doDiv()
   {
       double temp3=this.getNum1()/this.getNum2();
       this.SetNum3(temp3);
   }
   //dopower
    public void doPower()
   {
       double temp4=Math.pow(this.getNum1(),this.getNum2());
       this.SetNum3(temp4);
   }
   public String toString(){
       return (this.getNum1()+","+this.getNum2()+","+this.getNum3());
   }
   public double toMax()
   {
       double n3=Math.max(20, 30);
       return n3;
   }
    
}
