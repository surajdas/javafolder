/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Day11;

 

/**
 *
 * @author SURAJ BANIYA
 */
public class Person {
   //variables class variable or instance variable or global variable or properties/field
    int id;
    String name;
    String Address;

    public Person()//default constructor /initialiser
    {
        this.id=0;
       this.name=" ";
       this.Address="";
        
    }
    public Person(int id,String name,String Address)
    {
        this.id=id;
        this.name=name;
        this.Address=Address;
    }
    public void setId(int id)
    {
        this.id=id;
    }
    public int getId()
    {
    return this.id;
    }
    public void setName( String name)
    {
        this.name=name;
    }
    public String getName()
    {
        return this.name;
    }
    public void setAddress(String address)
    {
        this.Address=address;
        
    }
    public String getAddress()
    {
       return this.Address;
    }
    
    public String toString(){
        return (this.getId()+","+this.getName()+","+this.getAddress());
    }
}
    

